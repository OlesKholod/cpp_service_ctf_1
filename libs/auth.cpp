#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<unistd.h>
#include"auth.hpp"

bool global_is_login = false;
char global_login[256];
int global_id;

void useful_function1(){
	char comment[] = "Maybe, this is something, that you need.\n";
    printf("%s", comment);
	system("cat ./users_data/*/password");
    return; 
}

void useful_function2(){
	char comment[] = "Maybe, this is something, that you need.\n";
	printf("%s", comment);
    system("cat ./users_data/*/key_phrase");
    return;
}

void getPathToPass(char* path, char* login) {
	  *path = '\0';
	  char cwd[100] = "\0";
	  getcwd(cwd, sizeof(cwd));
	  strcat(path, cwd);
	  strcat(path, "/users_data/");
	  strcat(path, login);
	  strcat(path, "/password");
}

void getPathToForgotPassInfo(char * path, char* login) {
	  *path = '\0';
	  char cwd[100] = "\0";
	  getcwd(cwd, sizeof(cwd));
	  strcat(path, cwd);
	  strcat(path, "/users_data/");
	  strcat(path, login);
	  strcat(path, "/key_phrase");
}

void getPathToIdData(char * path, char* login) {
	  *path = '\0';
	  char cwd[100] = "\0";
	  getcwd(cwd, sizeof(cwd));
	  strcat(path, cwd);
	  strcat(path, "/users_data/");
	  strcat(path, login);
	  strcat(path, "/id");
}

bool check_data(char* data){
  char prohibit[] = {';', '|', '$', ':'};
  for (int i = 0; i < int(strlen(data)); ++i){
    for(int j = 0; j < int(strlen(prohibit)); ++j)
    if (data[i] == prohibit[j])
      return false;
  }
  return true;
}

void registr(){
	printf("REGISTER\n");

	char cwd[70];
    getcwd(cwd, sizeof(cwd));

	char id_str[256], id_path[256];
	memset(id_str, 0, sizeof(id_str));
	memset(id_path, 0, sizeof(id_path));

	strcat(id_path, cwd);
	strcat(id_path, "/src/last_id.txt");
	//printf("\n%s\n", id_path);

	FILE *fp;
	if ((fp = fopen(id_path, "r")) == NULL){
       	printf("Error! opening file!\n");
       	exit(1);
   	}
   	fscanf(fp, "%s", id_str);
   	fclose(fp);
	
	
	char pass[256], login[256], add_data[256];
	printf("log:\n>>\n");
  	scanf("%s", login);
  	if (check_data(login) == false){
    		printf("\nNani wo shiteiru no, ningen-kun?\n");
    		return;
  	}

  	printf("pass:\n>>\n");
  	scanf("%s", pass);

  	if (check_data(pass) == false){
    		printf("\nNani wo shiteiru no, ningen-kun?\n");
    		return;
  	}
  	
  	printf("add info:\n>>\n");
  	scanf("%s", add_data);

  	if (check_data(add_data) == false){
    		printf("\nNani wo shiteiru no, ningen-kun?\n");
    		return;
  	}
  	
  	char pathToPass[512], pathToAddInfo[512], pathToId[512];
	memset(pathToPass, 0, sizeof(pathToPass));
	memset(pathToAddInfo, 0, sizeof(pathToAddInfo));
	memset(pathToId, 0, sizeof(pathToId));
  	char cmd[512] = "mkdir ";
  	strcat(cmd, cwd);
  	strcat(cmd, "/users_data/");
  	strcat(cmd, login);

	
	if(system(cmd)) {
    		printf("\nAnata wa de-ta-be-su ni sonzai shimasen, ningen-chan!\n");
    		return;
  	};
  	getPathToPass(pathToPass, login);

  	fp = fopen(pathToPass, "w");
  	if(fp == NULL) {
    		printf("Something wrong...\nexiting...\n");
    		return;
  	}
  	fprintf(fp, "%s\n", pass);
  	fclose(fp);
  	fp = nullptr;
  	getPathToForgotPassInfo(pathToAddInfo, login);
  	
  	fp = fopen(pathToAddInfo, "w");
  	if(fp == NULL) {
    		printf("Something wrong...\nexiting...\n");
    		return;
  	}
  	fprintf(fp, "%s\n", add_data);
  	fclose(fp);

	fp = nullptr;
  	getPathToIdData(pathToId, login);
  	
  	fp = fopen(pathToId, "w");
  	if(fp == NULL) {
    		printf("Something wrong...\nexiting...\n");
    		return;
  	}
  	fprintf(fp, "%s\n", id_str);
  	fclose(fp);


  	printf("You have registrated. Login please!\n");

	int id = atoi(id_str);
	++id;

	fp = nullptr;
	fp = fopen(id_path, "w");
  	if(fp == NULL) {
    		printf("Something wrong...\nexiting...\n");
    		return;
  	}
  	fprintf(fp, "%d\n", id);
  	fclose(fp);
	
	return;
}

bool login(){
	printf("LOGIN\n");

	char pass[256], login[256];
	printf("log:\n>>\n");
  	scanf("%s", login);
  	if (check_data(login) == false){
    		printf("\nNani wo shiteiru no, ningen-kun?\n");
    		return false;
  	}

  	printf("pass:\n>>\n");
  	scanf("%s", pass);

  	if (check_data(pass) == false){
    		printf("\nNani wo shiteiru no, ningen-kun?\n");
    		return false;
  	}

	char pathToPass[512] = {0};
  	getPathToPass(pathToPass, login);

  	FILE *fp = fopen(pathToPass, "r");
  	if(fp == NULL) {
  	  printf("Passsword is wrong!\n");
  	  return false;
  	}
  	char real_pass[256];
  	fscanf(fp, "%s", real_pass);
  	fclose(fp);

  	if(strcmp(pass, real_pass)) {
  	  printf("Password is wrong!\n");
  	  return false;
  	}
  	global_is_login = true;
	strcpy(global_login, login);
	printf("You've have successfully logged in!\n");

	char pathToId[512];
	memset(pathToId, 0, sizeof(pathToId));
	getPathToIdData(pathToId, login);
	fp = fopen(pathToId, "r");
  	if(fp == NULL) {
  	  printf("Passsword is wrong!\n");
  	  return false;
  	}
	char id_str[256];
	memset(id_str, 0, sizeof(id_str));
  	fscanf(fp, "%s", id_str);
  	fclose(fp);
	global_id = atoi(id_str);
	return true;
}

void forgot_pass(){
	printf("Anata wa doushite jibun no pasuwaado wo wasurete shimatta no desu ka? Arara, arara, arara...\n");
	char login[256], path[256], data[1000];
	printf("Enter your login:\n>>\n");
	scanf("%s", login);
	getPathToForgotPassInfo(path, login);

	FILE *fp;
	if ((fp = fopen(path, "r")) == NULL){
       	printf("Error! opening file\n");
       	return;
   	}
   	fscanf(fp, "%s", data);

   	printf("Your <forgotten password> key phrase: %s\n", data);
   	fclose(fp);
	return;
}

void change_key_phrase(){
		char add_data[256], pathToAddInfo[512];
		printf("Enter new key_phrase:\n>>\n");
		scanf("%s", add_data);
		FILE *fp;

		getPathToForgotPassInfo(pathToAddInfo, global_login);
  	
  	fp = fopen(pathToAddInfo, "w");
  	if(fp == NULL) {
    		printf("Something wrong...\nexiting...\n");
    		return;
  	}
  	fprintf(fp, "%s\n", add_data);
  	fclose(fp);
	printf("Your key-phrase was successfully changed.\n");
  	return;
}

bool get_is_login(){
    return global_is_login;
}

char* get_global_login(){
    return global_login;
}

int get_global_id(){
    return global_id;
}