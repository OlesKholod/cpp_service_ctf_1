import socket
import subprocess
import threading
import errno
import signal
import sys
import os
import time
from tqdm import tqdm

DEBUG = False

def signal_handler(signal, frame):
    if DEBUG:
        print("\nYou've pressed comb `Ctrl+C!`")
    print("\nServer will be downed")
    for i in tqdm(range(30)):
        time.sleep(0.05)
    print("Server is down.")
    exit(0)

def handle_client(conn, addr):
    if DEBUG:
        print(f"[INFO] New connection from {addr}")
    # Start the program in interactive mode with the client
    p = subprocess.Popen(["./build/program"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    # Start a new thread to handle sending output to client
    def send_output():
        while p.poll() is None:  # While program is running
            try:
                conn.sendall(p.stdout.readline())
            except:
                if DEBUG:
                    print(f"[INFO] Connection from {addr} closed")
                p.terminate()
                p.wait()
                p.stdout.close()
                conn.close()
                return
        p.terminate()
        p.wait()
        p.stdout.close()
        conn.close()
        return

    out_thread = threading.Thread(target=send_output)
    out_thread.start()
    # Forward client input to program
    while True:
        try:
            data = conn.recv(4096)
            if DEBUG:
                print("DATA ", data)
            if not data:
                break
            p.stdin.write(data)
            #p.communicate(data)
            # output, error = p.communicate(data)
            # print(output.decode())
            try:
                p.stdin.flush()
            except BrokenPipeError as e:
                if e.errno != errno.EPIPE:
                    raise
                if DEBUG:
                    print("Broken pip encountered!")
                p.terminate()
                p.wait()
                p.stdout.close()
                #p.stdin.close()
                #p.stderr.close()
                if DEBUG:
                    print(f"[INFO] Connection from {addr} closed")
                conn.close()
                return
                break
        except ConnectionResetError as e:
            p.terminate()
            p.wait()
            p.stdout.close()
            if DEBUG:
                print(f"[INFO] Connection from {addr} closed")
            conn.close()
            return

    # Clean up program and socket
    p.stdin.close()
    p.wait()
    if DEBUG:
        print(f"[INFO] Connection from {addr} closed")
    conn.close()

def start_server(address:str = '127.0.0.1', port:int = 12345):
    print(f"Server is running(address - {address}, port - {port})")
    server_addr = (address, port)
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_sock.bind(server_addr)
    server_sock.listen(50)

    while True:
        conn, addr = server_sock.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)

    filename = "build/program"
    if os.path.exists(filename):
        if os.access(filename, os.X_OK):
            if DEBUG:
                print("File exists and it's executable")
        else:
            print("File exists, but it's not executable")
            exit(0)
    else:
        print("File doesn't exist")
        exit(0)

    if len(sys.argv) == 3:
        arguments = sys.argv[1:]
        #print(arguments[0], arguments[1])
        start_server(arguments[0], int(arguments[1]))
    else:
        if DEBUG:
            print("Usage is 'python3 server <ip_address> <port_number>'")
        start_server()