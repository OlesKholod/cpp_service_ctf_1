#ifndef DES_LIBRARY_H_INCLUDED
#define DES_LIBRARY_H_INCLUDED

#include <cstdlib>
#include <string>
#include <vector>

class DES{
private:
    std::string total_code, total_key;
    std::string tempCode_1, tempCode_2;
    std::string m_code, code;
    std::string L, R, C, D;
    std::vector<std::string> K;
public:
    DES() {};
    // encryption
    std::string encode(std::string str, std::string key);
    // decryption
    std::string decode(std::string str, std::string key);
    // fill string
    void fill(std::string str);
    // make foramt string
    void formatSourceCode();
    // change IP (LR)
    void getIP0();
    // Feistel function (round)
    std::string Feistel(std::string R, std::string K);
    // P exchange
    std::string getPTransform(std::string str);
    // Feistel SBOX
    std::string Feistel_SBOX(std::string str, int num);
    // Expanding E
    std::string E_expend(std::string str);
    // XOR
    std::string XORoperation(std::string a, std::string b);
    // Т iteration (encrypted)
    std::string iterationT_A(int begin, int end);
    // T iteration (decrypted)
    std::string iterationT_D(int begin, int end);
    // Обратная замена IP
    std::string getIP1(std::string str);
    // sort plaintext 1
    std::string formatResult(std::string str);
    // sort plaintext 2
    std::string formatAndReduceResult(std::string str);
    // from binary to decimal
    int Two2Ten(std::string num);
    // Key format
    std::string formatKey();
    // 
    std::string getPC1Key(std::string str);
    // 
    void get_C_D(std::string str);
    // 
    void getKeyI();
    // 
    void LS_1(std::string& str);
    // 
    void LS_2(std::string& str);
    // 
    std::string getPC2Key(std::string str);
    // 
    void getKeys();
};

std::string generate_DES_key(int n);

#endif //DES_LIBRARY_H_INCLUDED
