#include <iostream>
#include <sstream>
#include <string>
#include <cctype>
#include <cstring>
#include <algorithm>
#include <vector>
#include <cstddef>
#include <ctime>
#include <bitset>
#include "crypto.hpp"

//help functions

int gcd(int a, int b){
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

int partition(std::vector<int> &V, int low, int high){
    int pivot = V[high];
    int i = (low - 1);  
    for (int j = low; j <= high- 1; j++){
        if (V[j] <= pivot){
            i++;   
            std::swap(V[i], V[j]);
        }
    }
    std::swap(V[i + 1], V[high]);
    return (i + 1);
}

void quickSort(std::vector<int> &V, int low, int high){
    if (low < high){
        int pi = partition(V, low, high);
        quickSort(V, low, pi - 1);
        quickSort(V, pi + 1, high);
    }
}
 
void printArray(std::vector<int> V){
    for (int i = 0; i < V.size(); i++)
        std::cout << V[i] << ' ';
    std::cout << "\n";
}

std::string create_rand_string(int n){
    srand((unsigned) time(NULL));
    std::string res = "";
    static const char AlphaNumeric[] = "0123456789,./;:[]{}()|+=-_#@$"
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz";
    int MyLen = sizeof(AlphaNumeric) - 1;
    for (int i = 0; i < n; ++i){
        res += AlphaNumeric[rand() % MyLen];
    }
    return res;
}

std::vector<int> create_rand_int_arr(int n, int maxn){
    srand((unsigned) time(NULL));
    std::vector<int> res;
    int buf = 0, d;
    for (int i = 0; i < n; ++i){
        d = 0;
        while(!d){
            d = (rand() + 1) % maxn;
        }
        buf += d;
        res.push_back(buf);
    }
    quickSort(res, 0, n - 1);
    return res;
}

void prepare_text_for_Polyblus_sq(std::string &text){
    std::string buf;
    for (char c : text) {
        if (std::isalpha(c)) {
            buf += std::toupper(c);
        }
    }
    text = buf;
}

void print_vector(std::string str){
 
    std::byte bytes[str.size()];
    for (size_t i = 0; i < str.size(); i++) {
        bytes[i] = std::byte(str[i]);
    }
 
    for (auto &b: bytes) {
        std::cout << std::to_integer<int>(b) << ' ';
    }
    std::cout << std::endl;
}

std::vector<std::byte> str_to_bytes(std::string str){
    std::vector<std::byte> bytes;
    for (size_t i = 0; i < str.size(); i++) {
        bytes.push_back(std::byte(str[i]));
    }
    return bytes;
}

std::string from_bin_to_char(std::string data){
    std::string result;
    std::stringstream sstream(data);
    while(sstream.good()){
        std::bitset<8> bits;
        sstream >> bits;
        char c = char(bits.to_ulong());
        result += c;
    }

    return result;
}

//encryption/decryption functions and key generators functions

void XOR_enc(std::string &text, std::string key){
    int len_a = text.length() / key.length();
    int len_q = text.length() % key.length();
    std::string buf = "";
    for (int i = 0; i < len_a; ++i){
        buf += key;
    }
    for (int i = 0; i < len_q; ++i){
        buf += key[i];
    }
    for (int i = 0; i < text.length(); ++i){
        text[i] = text[i] ^ buf[i];
    }
    return;
}

void affine_cipher_enc(std::string &text, int a, int b, int m = 26){
    for (int i = 0; i < text.length(); ++i){
        if (text[i] >= 'a' && text[i] <= 'z')
            text[i] = ((a * (int(text[i]) - int('a')) + b) % m) + int('a');
        else if (text[i] >= 'A' && text[i] <= 'Z')
            text[i] = ((a * (int(text[i]) - int('A')) + b) % m) + int('A');
    }
    return;
}

void affine_cipher_dec(std::string &text, int a, int b, int m = 26){
    int nega = 1;
    while ((a * nega) % m != 1){
        ++nega;
    }
    //std::cout << "nega" << nega << std::endl;
    for (int i = 0; i < text.length(); ++i){
        if (text[i] >= 'a' && text[i] <= 'z')
            text[i] = ((nega * ((int(text[i]) - int('a')) - b + m)) % m) + int('a');
        else if (text[i] >= 'A' && text[i] <= 'Z')
            text[i] = ((nega * ((int(text[i]) - int('A')) - b + m)) % m) + int('A');
    }
    return;
}

void generate_affine_cypher_keys(int &keyA, int &keyB, int n){
    srand((unsigned) time(NULL));
    std::vector<int> keyB_var;
    for (int i = 1; i < n; ++i){
        if (gcd(i, n) == 1)
            keyB_var.push_back(i);
    }
    keyA = keyB_var[rand() % keyB_var.size()];
    keyB = rand() % n;
}

void vigenere_cipher_enc(std::string &text, std::string key, int m = 26){
    int len_a = text.length() / key.length();
    int len_q = text.length() % key.length();
    std::string buf = "";
    for (int i = 0; i < len_a; ++i){
        buf += key;
    }
    for (int i = 0; i < len_q; ++i){
        buf += key[i];
    }
    for (int i = 0; i < text.length(); ++i){
        if ((text[i] >= 'a' && text[i] <= 'z') && (buf[i] >= 'a' && buf[i] <= 'z'))
            text[i] = ((int(text[i]) - int('a') + int(buf[i]) - int('a')) % m) + int('a');
        else if ((text[i] >= 'a' && text[i] <= 'z') && (buf[i] >= 'A' && buf[i] <= 'Z'))
            text[i] = ((int(text[i]) - int('a') + int(buf[i]) - int('A')) % m) + int('a');
        else if ((text[i] >= 'A' && text[i] <= 'Z') && (buf[i] >= 'a' && buf[i] <= 'z'))
            text[i] = ((int(text[i]) - int('A') + int(buf[i]) - int('a')) % m) + int('A');
        else if ((text[i] >= 'A' && text[i] <= 'Z') && (buf[i] >= 'A' && buf[i] <= 'Z'))
            text[i] = ((int(text[i]) - int('A') + int(buf[i]) - int('A')) % m) + int('A');
    }
    return;
}

void vigenere_cipher_dec(std::string &text, std::string key, int m = 26){
    int len_a = text.length() / key.length();
    int len_q = text.length() % key.length();
    std::string buf = "";
    for (int i = 0; i < len_a; ++i){
        buf += key;
    }
    for (int i = 0; i < len_q; ++i){
        buf += key[i];
    }
    for (int i = 0; i < text.length(); ++i){
        if ((text[i] >= 'a' && text[i] <= 'z') && (buf[i] >= 'a' && buf[i] <= 'z'))
            text[i] = ((int(text[i]) - int('a') - int(buf[i]) + int('a') + 2*m) % m) + int('a');
        else if ((text[i] >= 'a' && text[i] <= 'z') && (buf[i] >= 'A' && buf[i] <= 'Z'))
            text[i] = ((int(text[i]) - int('a') - int(buf[i]) + int('A') + 2*m) % m) + int('a');
        else if ((text[i] >= 'A' && text[i] <= 'Z') && (buf[i] >= 'a' && buf[i] <= 'z'))
            text[i] = ((int(text[i]) - int('A') - int(buf[i]) + int('a') + 2*m) % m) + int('A');
        else if ((text[i] >= 'A' && text[i] <= 'Z') && (buf[i] >= 'A' && buf[i] <= 'Z'))
            text[i] = ((int(text[i]) - int('A') - int(buf[i]) + int('A') + 2*m) % m) + int('A');
    }
    return;
}

std::string generate_vigenere_cypher_keys(int n){
    srand((unsigned) time(NULL));
    std::string res = "";
    static const char AlphaNumeric[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz";
    int MyLen = sizeof(AlphaNumeric) - 1;
    for (int i = 0; i < n; ++i){
        res += AlphaNumeric[rand() % MyLen];
    }
    
    return res;
}

void scytala_enc(std::string &text, int n, int m){
    std::string buf = "";
    if (text.length() < n * m){
        int k = text.length();
        for (int i = 0; i < n * m - k; ++i){
            text.push_back('_');
        }
        for (int i = 0; i < n; ++i){
            for (int j = 0; j < m; ++j){
                buf += text[j * n + i];
            }
        }
    } else if (text.length() > n * m){
        int nm = n * m;
        int l = text.length() / nm;
        if(text.length() % nm != 0){
            int k = text.length() % nm;
        
            for (int i = 0; i < nm - k; ++i){
                text.push_back('_');
            }
        }
        std::string h = "";
        for (int p = 0; p <= l; ++p){
            for (int i = 0; i < n; ++i){
                for (int j = 0; j < m; ++j){
                    h += text[j * n + i + p * nm];
                }
            }
            buf += h;
            h.clear();
        }
    } else{
        for (int i = 0; i < n; ++i){
            for (int j = 0; j < m; ++j){
                buf += text[j * n + i];
            }
        }
    }
    text = buf;
}

void scytala_dec(std::string &text, int n, int m){
    std::string buf = "";
    for (int i = 0; i < m; ++i){
        for (int j = 0; j < n; ++j){
            buf += text[j * m + i];
        }
    }
    text = buf;
}

void generate_scytala_cypher_keys(int &keyN, int &keyM){

}

std::vector<std::vector<char>> create_Polyblus_sq(int mode){
    std::vector<std::vector<char>> res;
    std::vector<char> buf1 = {'A', 'B', 'C', 'D', 'E'};
    std::vector<char> buf2 = {'F', 'G', 'H', 'I', 'K'};
    std::vector<char> buf3 = {'L', 'M', 'N', 'O', 'P'};
    std::vector<char> buf4 = {'Q', 'R', 'S', 'T', 'U'};
    std::vector<char> buf5 = {'V', 'W', 'X', 'Y', 'Z'};
    res.push_back(buf1);
    res.push_back(buf2);
    res.push_back(buf3);
    res.push_back(buf4);
    res.push_back(buf5);
    return res;
}

void Polyblus_square_enc(std::string &text, int n){
    for (int i = 0; i < text.length(); ++i){
        if (text[i] == 'J'){
            text[i] = 'I';
        }
    }
    std::vector<std::vector<char>> Polyblus_sq = create_Polyblus_sq(0);

    if (n == 1){
        bool mark;
        for (int i = 0; i < text.length(); ++i){
            mark = false;
            for (int j1 = 0; j1 < Polyblus_sq.size(); ++j1){
                for (int j2 = 0; j2 < Polyblus_sq[j1].size(); ++j2){
                    if(text[i] == Polyblus_sq[j1][j2]){
                        text[i] = Polyblus_sq[(j1 + 1) % Polyblus_sq.size()][j2];
                        mark = true;
                        break;
                    }
                }
                if (mark) break;
            }
        }
    } else if(n == 2){
        std::string buf = "";
        std::vector<int> ver, hor;
        bool mark;
        for (int i1 = 0; i1 < text.length(); ++i1){
            mark = false;
            for (int j1 = 0; j1 < Polyblus_sq.size(); ++j1){
                for (int j2 = 0; j2 < Polyblus_sq[j1].size(); ++j2){
                    if(text[i1] == Polyblus_sq[j1][j2]){
                        hor.push_back(j1);
                        ver.push_back(j2);
                        mark = true;
                        break;
                    }
                }
                if (mark) break;
            }
        }
        
        for (int i = 0; i < ver.size() - 1; i += 2){
            buf += Polyblus_sq[ver[i + 1]][ver[i]];
        }
        if (ver.size() & 1 == 1){
            buf += Polyblus_sq[0][ver.size() - 1];
            for (int i = 1; i < hor.size(); i += 2){
                buf += Polyblus_sq[hor[i + 1]][hor[i]];
            }
        } else {
            for (int i = 0; i < hor.size() - 1; i += 2){
                buf += Polyblus_sq[hor[i + 1]][hor[i]];
            }
        }
        text = buf;
    } else if(n == 3){
        std::string buf = "";
        std::vector<int> ver, hor;
        bool mark;
        for (int i = 0; i < text.length(); ++i){
            mark = false;
            for (int j1 = 0; j1 < Polyblus_sq.size(); ++j1){
                for (int j2 = 0; j2 < Polyblus_sq[j1].size(); ++j2){
                    if(text[i] == Polyblus_sq[j1][j2]){
                        ver.push_back(j2);
                        hor.push_back(j1);
                        mark = true;
                        break;
                    }
                }
                if (mark) break;
            }
        }
        for (int i = 0; i < hor.size(); ++i){
            ver.push_back(hor[i]);
        }
        for (int i = 1; i < ver.size() + 1; i += 2){
            buf += Polyblus_sq[ver[(i + 1) % ver.size()]][ver[i % ver.size()]];
        }
        text = buf;
    } else {
        std::cout << "What do you do?\n"; 
    }
}

void Polyblus_square_dec(std::string &text, int n){
    if (n == 1){
        for (int i = 0; i < 4; ++i){
            Polyblus_square_enc(text, n);
        }
    } else if(n == 2){
        for (int i = 0; i < 3; ++i){
            Polyblus_square_enc(text, n);
        }
    } else if(n == 3){
        for (int i = 0; i < 19; ++i){
            Polyblus_square_enc(text, n);
        }
    } else {
        std::cout << "What do you do?\n"; 
    }
}

void cardan_grille(std::string &text, std::vector<int> a, int len_enc){
    std::string res = "";
    int cur_pos, next_pos, len_frag;
    cur_pos = 0;
    //int j = 0;
    for (int i = 0; i < a.size(); i++){
        next_pos = a[i];
        len_frag = next_pos - cur_pos - 1;
        res += create_rand_string(len_frag);
        res += text[i];
        cur_pos = a[i];
    }
    if (len_enc > a[a.size() - 1]){
        res += create_rand_string(a[a.size() - 1] - len_frag);
    }
    text.clear();
    text = res;
}

void get_str_from_cardan_grille(std::string &text, std::vector<int> a){
    std::string buf = "";
    for (int i = 0; i < a.size(); ++i){
        buf += text[a[i] - 1];
    }
    text = buf;
}

std::vector<int> generate_cardan_grille_keys(std::string A){
    std::vector<int> data = create_rand_int_arr(A.length(), 20);
    return data;
}

void rabin_enc(std::string &text, int len_enc, int *a, int n){

}
